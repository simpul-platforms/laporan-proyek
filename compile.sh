# generate aux file
pdflatex -output-directory=output laporan-achmad.tex && cp daftar-pustaka.bib output
# cant use `bibtex output/template-skripsi` somehowgi
cd output
# add bib
bibtex laporan-achmad
cd ..
# when bib added, its need to be double compiled
pdflatex -output-directory=output laporan-achmad.tex
pdflatex -output-directory=output laporan-achmad.tex
## Laporan Program Latihan Akademik
## Web Service SIMPUL
=============================

Dibuat dengan [Template Naskah Skripsi dengan typesetting LaTeX untuk Universitas Pendidikan Indonesia](https://gitlab.com/amaceh/template-skripsi-upi-latex) Disesuaikan dengan Pedoman Penulisan Karya Ilmiah UPI 2018. Dimodifikasi dari [Template Skripsi JTETI Universitas Gadjah Mada](https://github.com/gtrdp/template-skripsi).

Semoga bermanfaat. Anda sangat dibolehkan untuk turut berkontribusi dalam project ini dengan *Fork*, *Pull Request*, *Create New Issue*, atau turut menjadi kontributor repo ini.

Terimakasih.

Quick Start
-----------
1. Siapkan LaTeX environment pada komputer anda, begitu pula LaTeX editornya. File yang diperlukan biasanya berukuran besar, jadi siapkan koneksi internet yang relatif lancar.
2. Clone/Unduh repository ini.
3. Mulai tulis naskah anda, keterangan dari masing-masing file dalam template ini ada di bawah.
4. Compile dengan eksekusi file compile.sh
5. Pertamakali pakai LaTeX? Butuh bantuan? Pergunakanlah Google dengan baik dan bijak.

Contents
--------
Berikut penjelasan dari file-file utama dalam template ini. File lain yang tidak tercantum hanya pelengkap dalam repository ini.

		template-skripsi/
			├── gambar/
			│	   ├── logo-upi-resmi.png
			│	   └── wsn.png
			├── output/
			       └── laporan-achmad.pdf		
			├── bab1.tex
			├── bab2.tex
			├── bab3.tex
			├── bab4.tex
			├── bab5.tex
			├── compile.sh
			├── daftar-pustaka.bib
			├── laporan-achmad.tex
			└── jtetiskripsi.cls

### bab1.tex - bab5.tex
Konten utama dari skripsi, mulai dari BAB I (pendahuluan) sampe BAB V (kesimpulan). Silakan disesuaikan dengan jumlah bab skripsi anda, hapus file yang tidak perlu atau tambahkan file baru untuk bab baru.

### daftar-pustaka.bib
File yang berisi daftar referensi-referensi yang anda gunakan dalam skripsi. File ini penting guna menyusun daftar pustaka anda. Dengan file ini menyusun daftar pustaka menjadi sangat sangat sangat mudah.

File ini adalah hasil export dari aplikasi *reference management* seperti Mendeley, Zotero, EndNote, dll. Biasakan mengorganisir referensi skripsi anda menggunakan aplikasi *reference management*.

### laporan-achmad.tex
File ini laporan-achmad.tex adalah file utama (kepala) dari laporan. Berisi informasi-informasi dasar, seperti judul skripsi, nama penulis, nama pembimbing, dll.

### laporan-achmad.pdf
File ini adalah laporan dalam bentuk matang. Sudah rapi dan dapat dicetak untuk dijilid. File ini di-*generate* secara otomatis menggunakan LaTeX.

### jtetiskripsi.cls
File yang berisi aturan-aturan format skripsi. Contoh, format cover, halaman pengesahan, daftar isi, daftar pustaka, dan konten skripsi.

Jika anda ingin memodifikasi template skripsi ini, ubahlah file *jtetiskripsi.cls* ini.

### compile.sh
berisi perintah untuk kompilasi source code. eksekusi dengan cara `$ ./compile.sh` pada terminal linux. untuk OS windows rename file menjadi `compile.bat` dan ubah perintah `cp` menjadi perintah yang valid di windows (perintah untuk menyalin file daftar pustaka)

### gambar/
Masukkan gambar-gambar pada skripsi anda di folder ini. Gambar default: logougm.png (dipakai di cover) dan wsn.png (hanya dipakai di template awal, silakan dihapus jika tidak diperlukan).

### output/
Direktori kosong ini digunakan untuk menyimpan hasil kompilasi. supaya file kompilasi tidak bercampur dengan source code.